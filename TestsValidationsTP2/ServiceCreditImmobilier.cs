﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestsValidationsTP2
{
    public class ServiceCreditImmobilier
    {
        public static double CalculMensualite(float capital, int durree, TypeTaux typeTaux, float tauxAssurance, int anneeDiffere)
        {
            CreditImmobilier c;
            if (anneeDiffere == 0)
            {
                c = new CreditImmobilier(TypeEmprunt.Total, capital, durree, typeTaux, tauxAssurance);
            } else
            {
                c = new CreditImmobilier(TypeEmprunt.Differe, capital, durree, typeTaux, tauxAssurance, anneeDiffere);
            }
            return Math.Round(c.CalculMensualite(),2);
        }

        public static double CalculCoutTotalCredit(float capital, int durree, TypeTaux typeTaux, float tauxAssurance, int anneeDiffere)
        {
            CreditImmobilier c;
            if (anneeDiffere == 0)
            {
                c = new CreditImmobilier(TypeEmprunt.Total, capital, durree, typeTaux, tauxAssurance);
            }
            else
            {
                c = new CreditImmobilier(TypeEmprunt.Differe, capital, durree, typeTaux, tauxAssurance, anneeDiffere);
            }
            return Math.Round(c.CalculCoutTotalCredit(),2);
        }

        public static double CalculMensualiteAssurance(float capital, int durree, TypeTaux typeTaux, float tauxAssurance, int anneeDiffere)
        {
            CreditImmobilier c;
            if (anneeDiffere == 0)
            {
                c = new CreditImmobilier(TypeEmprunt.Total, capital, durree, typeTaux, tauxAssurance);
            }
            else
            {
                c = new CreditImmobilier(TypeEmprunt.Differe, capital, durree, typeTaux, tauxAssurance, anneeDiffere);
            }
            return Math.Round(c.CalculMensualiteAssurance(), 2);
        }
    }
}
