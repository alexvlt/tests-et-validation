﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestsValidationsTP2
{
    public class CreditImmobilier
    {
        private double capital;
        private int durree;
        private float tauxAssurance;
        private float tauxInteret;
        private TypeEmprunt typeEmprunt;
        private int anneeDiffere;
        private double mensualite;

        public CreditImmobilier(TypeEmprunt _typeEmprunt, double _capital, int _durree, TypeTaux _typeTaux, float _tauxAssurance, int _anneeDiffere = 0)
        {
            this.capital = _capital;
            this.durree = _durree;
            this.tauxAssurance = _tauxAssurance;
            this.typeEmprunt = _typeEmprunt;
            this.anneeDiffere = _anneeDiffere;

            switch (_typeTaux)
            {
                case TypeTaux.Bon:
                    if (durree >= 108 && durree < 120)
                    {
                        this.tauxInteret = 0.0062f;
                    }
                    else if (durree >= 120 && durree < 180)
                    {
                        this.tauxInteret = 0.0067f;
                    }
                    else if (durree >= 180 && durree < 240)
                    {
                        this.tauxInteret = 0.0085f;
                    }
                    else if (durree >= 240 && durree < 300)
                    {
                        this.tauxInteret = 0.0104f;
                    }
                    else
                    {
                        this.tauxInteret = 0.0127f;
                    }
                    break;

                case TypeTaux.TresBon:
                    if (durree >= 108 && durree < 120)
                    {
                        this.tauxInteret = 0.0043f;
                    }
                    else if (durree >= 120 && durree < 180)
                    {
                        this.tauxInteret = 0.0055f;
                    }
                    else if (durree >= 180 && durree < 240)
                    {
                        this.tauxInteret = 0.0073f;
                    }
                    else if (durree >= 240 && durree < 300)
                    {
                        this.tauxInteret = 0.0091f;
                    }
                    else
                    {
                        this.tauxInteret = 0.0115f;
                    }
                    break;

                case TypeTaux.Excellent:
                    if (durree >= 108 && durree < 120)
                    {
                        this.tauxInteret = 0.0035f;
                    }
                    else if (durree >= 120 && durree < 180)
                    {
                        this.tauxInteret = 0.0045f;
                    }
                    else if (durree >= 180 && durree < 240)
                    {
                        this.tauxInteret = 0.0058f;
                    }
                    else if (durree >= 240 && durree < 300)
                    {
                        this.tauxInteret = 0.0073f;
                    }
                    else
                    {
                        this.tauxInteret = 0.0089f;
                    }
                    break;
            }
        }

        public double CalculMensualite()
        {
            int durree = this.durree;

            if (this.typeEmprunt == TypeEmprunt.Differe)
                durree -= (this.anneeDiffere*12);

            double numerator = this.capital * this.tauxInteret / 12;
            double pow = Math.Pow(1 + (this.tauxInteret / 12), -durree);
            this.mensualite = numerator / (1 - pow);
            return this.mensualite;
        }

        public double CalculCoutTotalMensualite()
        {
            int durree = this.durree;

            if (this.typeEmprunt == TypeEmprunt.Differe)
                durree -= (this.anneeDiffere * 12);

            return CalculMensualite() * durree - this.capital;
        }

        public double CalculMensualiteAssurance()
        {
            return this.capital*this.tauxAssurance/12;
        }

        public double CalculCoutTotalAssurance()
        {
            return CalculMensualiteAssurance() * this.durree;
        }

        public double CalculCoutTotalCredit()
        {
            //Différé, on ne paie que les interet + Assurance la 1ère année
            double interet = Math.Round(this.capital * (this.tauxInteret / 12)*(this.anneeDiffere*12),2);
            return CalculCoutTotalMensualite() + CalculCoutTotalAssurance() + interet;
        }
    }
}
