﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestsValidationsTP2;

namespace TestsValidationsTP2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9.-]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void IntegerValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btnCalculMensualite_Click(object sender, RoutedEventArgs e)
        {
            float tauxAssurance = 0.003f;

            if (cmbBoxSportif.SelectedIndex == 1)
                tauxAssurance -= 0.0005f;

            if (cmbBoxFumeur.SelectedIndex == 1)
                tauxAssurance += 0.0015f;

            if (cmbBoxProblemeCardiaque.SelectedIndex == 1)
                tauxAssurance += 0.003f;

            if (cmbBoxIngenieurInformatique.SelectedIndex == 1)
                tauxAssurance -= 0.0005f;

            if (cmbBoxPiloteChasse.SelectedIndex == 1)
                tauxAssurance += 0.0015f;

            int durree;

            if (!int.TryParse(txtBoxDurree.Text, out durree))
                return;

            if (durree < 108 || durree > 300)
                return;

            float capital;

            if (!float.TryParse(txtBoxCapital.Text, out capital))
                return;

            if (capital < 50000)
                return;

            int anneeDifferre = 0;

            if (!int.TryParse(txtBoxAnneeDiffere.Text, out anneeDifferre))
                return;


            TypeTaux typeTaux = TypeTaux.Bon;

            if (cmbBoxTypeTaux.SelectedIndex == 0)
                typeTaux = TypeTaux.Bon;

            if (cmbBoxTypeTaux.SelectedIndex == 1)
                typeTaux = TypeTaux.TresBon;

            if (cmbBoxTypeTaux.SelectedIndex == 2)
                typeTaux = TypeTaux.Excellent;

            double mensualite = ServiceCreditImmobilier.CalculMensualite(capital, durree, typeTaux,tauxAssurance, anneeDifferre);
            lblMensualite.Content = mensualite.ToString() + " €";

            double cotisationMensuelAssurance = ServiceCreditImmobilier.CalculMensualiteAssurance(capital, durree, typeTaux, tauxAssurance, anneeDifferre);
            lblCotisationAssurance.Content = cotisationMensuelAssurance.ToString() + " €";

            double coutTotalCredit = ServiceCreditImmobilier.CalculCoutTotalCredit(capital, durree, typeTaux, tauxAssurance, anneeDifferre);
            lblCoutTotal.Content = coutTotalCredit.ToString() + " €";
        }
    }
}
