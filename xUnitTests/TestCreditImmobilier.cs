using System;
using TestsValidationsTP2;
using Xunit;

namespace xUnitTests
{
    public class TestCreditImmobilier
    {
        [Fact]
        public void TestCreditImmoTotal1()
        {
            double capital = 100000;
            int durree = 120;
            TypeTaux t = TypeTaux.Bon; //0.67
            float tauxAssurance = 0.003f;
            CreditImmobilier c = new CreditImmobilier(TypeEmprunt.Total, capital, durree, t, tauxAssurance);
            Assert.Equal(861.73, Math.Round(c.CalculMensualite(),2));
            Assert.Equal(25, Math.Round(c.CalculMensualiteAssurance(),2));
            Assert.Equal(6407.61, Math.Round(c.CalculCoutTotalCredit(),2));
        }

        [Fact]
        public void TestCreditImmoTotal2()
        {
            double capital = 150000;
            int durree = 240;
            TypeTaux t = TypeTaux.Excellent; //0.73
            float tauxAssurance = 0.004f;
            CreditImmobilier c = new CreditImmobilier(TypeEmprunt.Total, capital, durree, t, tauxAssurance);
            Assert.Equal(671.93, Math.Round(c.CalculMensualite(),2));
            Assert.Equal(50, Math.Round(c.CalculMensualiteAssurance(),2));
            Assert.Equal(23263.95, Math.Round(c.CalculCoutTotalCredit(),2));
        }

        [Fact]
        public void TestCreditImmoTotal3()
        {
            double capital = 200000;
            int durree = 300; //25 ans
            TypeTaux t = TypeTaux.TresBon; //1.15
            float tauxAssurance = 0.003f;
            CreditImmobilier c = new CreditImmobilier(TypeEmprunt.Total, capital, durree, t, tauxAssurance);
            Assert.Equal(767.41, Math.Round(c.CalculMensualite(),2));
            Assert.Equal(50, Math.Round(c.CalculMensualiteAssurance(),2));
            Assert.Equal(45222.91, Math.Round(c.CalculCoutTotalCredit(),2));
        }

        [Fact]
        public void TestCreditImmoDiffere1()
        {
            double capital = 100000;
            int durree = 120;
            TypeTaux t = TypeTaux.Excellent; //0.45
            float tauxAssurance = 0.004f;
            CreditImmobilier c = new CreditImmobilier(TypeEmprunt.Differe, capital, durree, t, tauxAssurance,1);
            Assert.Equal(944.9, Math.Round(c.CalculMensualite(),2));
            Assert.Equal(33.33, Math.Round(c.CalculMensualiteAssurance(),2));
            Assert.Equal(6498.77, Math.Round(c.CalculCoutTotalCredit(),2));
        }

        [Fact]
        public void TestCreditImmoDiffere2()
        {
            double capital = 100000;
            int durree = 120;
            TypeTaux t = TypeTaux.Bon; //0.67
            float tauxAssurance = 0.003f;
            CreditImmobilier c = new CreditImmobilier(TypeEmprunt.Differe, capital, durree, t, tauxAssurance,2);
            Assert.Equal(1070.04, Math.Round(c.CalculMensualite(),2));
            Assert.Equal(25, Math.Round(c.CalculMensualiteAssurance(),2));
            Assert.Equal(7064.15, Math.Round(c.CalculCoutTotalCredit(),2));
        }

        [Fact]
        //Test pour v�rifier qu'un credit total est �gale � un cr�dit diff�r� sur 0 ann�e.
        public void TestCreditImmoTotalDiffere()
        {
            double capital = 100000;
            int durree = 120;
            TypeTaux t = TypeTaux.Excellent; //0.45
            float tauxAssurance = 0.004f;
            CreditImmobilier c = new CreditImmobilier(TypeEmprunt.Differe, capital, durree, t, tauxAssurance,0);
            CreditImmobilier c2 = new CreditImmobilier(TypeEmprunt.Total, capital, durree, t, tauxAssurance,0);
            Assert.Equal(c.CalculMensualite(), c2.CalculMensualite());
            Assert.Equal(c.CalculMensualiteAssurance(), c2.CalculMensualiteAssurance());
            Assert.Equal(c.CalculCoutTotalCredit(), c2.CalculCoutTotalCredit());
        }

        [Fact]
        public void TestCreditImmoTotalDiffere2()
        {
            double capital = 100000;
            int durree = 120;
            TypeTaux t = TypeTaux.Excellent; //0.45
            float tauxAssurance = 0.004f;
            CreditImmobilier c = new CreditImmobilier(TypeEmprunt.Differe, capital, durree, t, tauxAssurance, 1);
            CreditImmobilier c2 = new CreditImmobilier(TypeEmprunt.Total, capital, durree, t, tauxAssurance, 0);
            Assert.NotEqual(c.CalculMensualite(), c2.CalculMensualite());
            Assert.NotEqual(c.CalculCoutTotalCredit(), c2.CalculCoutTotalCredit());
        }
    }
}
